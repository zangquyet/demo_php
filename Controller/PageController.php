<?php 
class PageController
{
	private $objUser;
	private $objProvince;

	public function __construct()
	{
		$this->objUser=new User();
		$this->objProvince=new Province();
	}
	/**
	 * Trang chủ
	 * @return HOMEPAGE TEMPLATE
	 */
	public function index()
	{
		$this->form_register();
	}
	/**
	 * Form đăng ký
	 * @return REGISTER FORM TEMPLATE
	 */
	public function form_register()
	{
		$provinces=$this->objProvince->get_All();
		include 'Views/register/register_form.php';
	}

	/**
	 * Danh sách đăng ký
	 * @return REGISTER LIST
	 */
	public function list_register()
	{
		/**
		 * Lấy danh sách người dùng
		 * @var User
		 */
		$users=$this->objUser->get_All();

		include 'Views/register/register_list.php';
	}
	public function store()
	{
		 /**
		  * Validate
		  */
		 
		 /**
		  * Lưu vào CSDL
		  */
		 $this->objUser->insert($_POST);
		 FlashMessage::add('Đăng ký thành công');
		 redirect('?controller=page&action=list_register');
	}
	/**
	 * Hiển thị trang chỉnh sửa thông tin người dùng 
	 */
	public function edit()
	{
		/**
		 * Kiểm tra nếu không truyền ID vào QUERY STRING
		 */
		if (empty($_GET['id'])) {
			/**
			 * Chuyển hướng về trang danh sách người dùng
			 */
			redirect('?controller=page&action=list_register');
		}else{
			$id=$_GET['id'];

			/**
			 * Lấy ra người dùng có ID bằng id từ QUERY STRING
			 */
			$user=$this->objUser->get_One($id);
			/**
			 * Kiểm tra người dùng có tồn tại trong CSDL hay không
			 */
			if ($user!=NULL) {
				/**
				 * Hiện thị form chỉnh sửa người dùng
				 */
				$provinces=$this->objProvince->get_All($id);
				include 'Views/register/register_edit.php';     
			}
		}
	}
	/**
	 * Xử lý cập nhập thông tin người dùng đươc gửi từ form pagecontroller@edit
	 */
	public function update()
	{
		/**
		 * Kiểm tra nếu không truyền ID vào QUERY STRING
		 */
		if (empty($_GET['id'])) {
			/**
			 * Chuyển hướng về trang danh sách người dùng
			 */
			redirect('?controller=page&action=list_register');
		}else{
			$id=$_GET['id'];

			/**
			 * Lấy ra người dùng có ID bằng id từ QUERY STRING
			 */
			$user=$this->objUser->get_One($id);

			/**
			 * Kiểm tra nếu người dùng có tồn tại trong CSDL
			 */
			if ($user!=NULL) {

				FlashMessage::add('Cập nhật thành công');

				/**
				 * Cập nhật thông tin người dùng
				 */
				$this->objUser->update($_POST,$id);  
				/**
				 * Chuyển hướng về trang chỉnh sửa người dùng
				 */
				redirect("?controller=page&action=edit&id=".$id);
			}
		}
	}

	/**
	 * Xóa một người dùng theo ID
	 */
	public function delete()
	{
		/**
			 * Chuyển hướng về trang danh sách người dùng
			 */
		if (empty($_GET['id'])) {
			redirect('?controller=page&action=list_register');
		}else{
			$id=$_GET['id'];
			/**
			 * Lấy ra người dùng có ID bằng id từ QUERY STRING
			 */
			$user=$this->objUser->get_One($id);
			/**
			 * Kiểm tra nếu người dùng có tồn tại trong CSDL
			 */
			if ($user!=NULL) {
				FlashMessage::add('Xóa thành công');
				/**
				 * Xóa người dùng có ID trùng với ID có trên QUERY STRING
				 */
				$this->objUser->delete($id);  
				redirect("?controller=page&action=list_register");
			}else{
				echo "Bản ghi không tồn tại";
			}
		}
	}
}
?>