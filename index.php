<?php 
session_start();
	/**
	 * Hàm autoload tự động include file php vào khi khởi tạo đối tượng  mới
	 */
	spl_autoload_register(function ($class_name) {
		/**
		 * Thư mục muốn autoload khi khởi tạo đối tượng
		 * @var string
		 */
		$folder='classes/';
	    include $folder.$class_name . '.php';
	});
include 'constant/constant.php';
include 'constant/helper.php';
include 'constant/FlashMessage.php';
$objMain=new Main();
$objMain->init();
 ?>