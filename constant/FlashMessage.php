<?php 
class FlashMessage {
    /**
     * Hiện thị SESSION sau đó xóa SESSION
     */
    public static function render() {
        if (!isset($_SESSION['messages'])) {
            return null;
        }
        $messages = $_SESSION['messages'];
        unset($_SESSION['messages']);
        return implode('<br/>', $messages);
    }
    /**
     * Thêm messge vào SESSION 
     */
    public static function add($message) {
        if (!isset($_SESSION['messages'])) {
            $_SESSION['messages'] = array();
        }
        $_SESSION['messages'][] = $message;
    }

}

?>