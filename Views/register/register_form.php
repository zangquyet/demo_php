<?php 
include './Views/partials/header.php';
?>
<script>
	
	$(document).ready(function() {
		$('#dob').datepicker({
});
	});	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			
			<form action="<?php echo base_url.'/?controller=page&action=store' ?>" enctype="multipart/form-data" method="POST" role="form">
				<legend>Đăng ký</legend>

			
				<div class="form-group">
					<label for="">Username</label>
					<input type="text" name="username" class="form-control" id="" >
				</div>	

						
				<div class="form-group">
					<label for="">Password</label>
					<input type="text" name="password" class="form-control" id="" >
				</div>

				<div class="form-group">
					<label for="">Email</label>
					<input name="email" type="text" class="form-control" id="" >
				</div>


				<div class="form-group">
					<label for="">Mobile</label>
					<input name="mobile" type="text" class="form-control" id="" >
				</div>


				<div class="form-group">
					<label for="">Birthday</label>
					<input name="birthday" type="text" class="form-control" id="dob" >
				</div>

				<div class="form-group">
					<div class="radio">
						<label>
							<input type="radio" name="gender" id="" value="1" checked="checked">
							Male
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="gender" id="" value="0" >
							Female
						</label>
					</div>
				</div>
	

				<div class="form-group">
					<label for="">Province</label>
					<select name="province_id" id="input" class="form-control" required="required">
						<option value="">Chọn</option>
						<?php foreach ($provinces as $key => $province): ?>
							<option value="<?php echo $province['id'] ?>"><?php echo $province['province_name'] ?></option>
						<?php endforeach ?>
					</select>
				</div>
	

				<div class="form-group">
					<label for="">Description</label>

					<textarea name="description" id="input" class="form-control ckeditor" rows="3" required="required"></textarea>
				</div>

				
			
				<button type="submit" class="btn btn-primary">Đăng ký</button>
			</form>
		</div>
	</div>
</div>

<?php 
include './Views/partials/footer.php';
?>