 <?php 
include './Views/partials/header.php';
?>
<script>
	
	$(document).ready(function() {
		$('#dob').datepicker({
});
	});	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<a href="<?=base_url.'?controller=page&action=list_register'?>" class="btn btn-success"><i class="fa fa-angle-left" aria-hidden="true"></i> Trở lại danh sách</a>
			
			<form action="<?php echo base_url.'/?controller=page&action=update&id='.$user['id'] ?>" enctype="multipart/form-data" method="POST" role="form">
				<legend>Cập nhật thông tin người dùng</legend>

			
				<div class="form-group">
					<label for="">Username</label>
					<input type="text" value='<?=$user['username']?>' name="username" class="form-control" id="" >
				</div>	

				<div class="form-group">
					<label for="">Email</label>
					<input name="email" value='<?=$user['email']?>' type="text" class="form-control" id="" >
				</div>


				<div class="form-group">
					<label for="">Mobile</label>
					<input name="mobile" value='<?=$user['mobile']?>' type="text" class="form-control" id="" >
				</div>


				<div class="form-group">
					<label for="">Birthday</label>
					<input name="birthday" value='<?=$user['birthday']?>' type="text" class="form-control" id="dob" >
				</div>

				<div class="form-group">
					<div class="radio">
						<label>
							<input <?=$user['gender']==1?'checked':''?> type="radio" name="gender" id="" value="1" >
							Male
						</label>
					</div>
					<div class="radio">
						<label>
							<input <?=$user['gender']==0?'checked':''?> type="radio" name="gender" id="" value="0" >
							Female
						</label>
					</div>
				</div>
	

				<div class="form-group">
					<label for="">Province</label>
					<select name="province_id" id="input" class="form-control" required="required">
						<option value="">Chọn</option>
						<?php foreach ($provinces as $key => $province): ?>
							<option <?=$user['province_id']==$province['id']?'selected':''?> value="<?php echo $province['id'] ?>"><?php echo $province['province_name'] ?></option>
						<?php endforeach ?>
					</select>
				</div>
	

				<div class="form-group">
					<label for="">Description</label>

					<textarea name="description" id="input" class="form-control ckeditor" rows="3" required="required"><?=$user['description']?></textarea>
				</div>

				
			
				<button type="submit" class="btn btn-primary">Update</button>
			</form>
		</div>
	</div>
</div>

<?php 
include './Views/partials/footer.php';
?>