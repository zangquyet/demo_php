<?php 
include './Views/partials/header.php';
?>
<script>
	
	$(document).ready(function() {
		$('#dob').datepicker({
});
	});	
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<h3>Danh sách người dùng</h3>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Username</th>
							<th>Email</th>
							<th>Mobile</th>
							<th>Birthday</th>
							<th>Province</th>
							<th>Gender</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($users as $key => $user): ?>
							<tr>
								<td><?php echo $user['id'] ?></td>
								<td><?php echo $user['username'] ?></td>
								<td><?php echo $user['email'] ?></td>
								<td><?php echo $user['mobile'] ?></td>
								<td><?php echo $user['birthday'] ?></td>
								<td><?php echo $user['province_name'] ?></td>
								<td><?php echo $user['gender']==1?'Nam':'Nữ' ?></td>
								<td>
									<button onclick="deleteUser(<?=$user['id']?>,'<?=$user['username']?>')" type="button" class="btn btn-warning btn-xs"><i class="fa fa-trash"></i> Xóa</button>
									<a href="<?php echo base_url ?>/?controller=page&action=edit&id=<?=$user['id']?>" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i> Chỉnh sửa</a>
								</td>
							</tr>
						<?php endforeach ?>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
	function deleteUser(id,user) {
		swal({
  title: "Xóa người dùng ? "+user,
  text: "Một khi bạn đã xóa người dùng thì dữ liệu không thể lấy lại được nữa!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    window.location.assign('<?=base_url.'?controller=page&action=delete&id='?>'+id);
  } else {
    //swal("Hủy xóa");
  }
});
	}
</script>
<?php 
include './Views/partials/footer.php';
?>