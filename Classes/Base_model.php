<?php 
/**
* 	
*/
class Base_model 
{
	protected  $table='';

	/**
	 * Lấy ra danh sách tòa bộ bản ghi
	 * @return [array] [Danh sách toàn bộ bản ghi]
	 */
	public function get_All()
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM $this->table";
		$result=mysqli_query($link,$sql);
		Db::close($link);
		$list=array();
		while ($row=mysqli_fetch_assoc($result)) {
			$list[]=$row;
		}
		return $list;	
	}



	/**
	 * Lấy ra 1 bản ghi
	 * @param  [Integer] $id [ID bản ghi cần lấy ra]
	 * @return [Array]     [trả về mảng 1 chiều chứa thông tin của bản ghi ]
	 */
	public function get_One($id)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM $this->table WHERE id=$id";
		$result=mysqli_query($link,$sql);
		Db::close($link);
		$list=array();
		$rs=mysqli_fetch_assoc($result);
		return $rs;
	}

	/**
	 * Chèn một bản ghi vào CSDL
	 * @param  array  $data [Mảng 2 chiều chứa thông tin cần chèn vào cơ sở dữ liệu]
	 * Ví dụ
	 *
	 * [
	 * 		'username'=>'Quyet Tran',
	 * 		'email'=>'quyettv.it@gmail.com'
	 * ]
	 * 
	 * @return [BOOLEAN]       [TRUE|FALSE]
	 */
	public function insert($data=[])
	{
		$link=Db::getInstance();

		$column=[];
		$value=[];
		foreach ($data as $key => $v) {
			$column[]=$key;
			$value[]="'$v'";
		}
		$column=implode(',', $column);
		$value=implode(',', $value);
		$sql="INSERT INTO $this->table ($column)
			VALUES ($value)";
		$result=mysqli_query($link,$sql);
		Db::close($link);
	}


	/**
	 * Cập nhật một bản ghi từ ID
	 * @param  array  $data [Mảng 2 chiều chứa thông tin cần cập nhật vào cơ sở dữ liệu]
	 * Ví dụ
	 *
	 * [
	 * 		'username'=>'Quyet Tran',
	 * 		'email'=>'quyettv.it@gmail.com'
	 * ]
	 *
	 * 
	 * @param  [INTERGER] $id   [ID bản ghi cần cập nhật vào CSDL]
	 * @return [BOOLEAN]       [TRUE|FALSE]
	 */
	public function update($data=[],$id)
	{
		$link=Db::getInstance();

		$update=[];
		foreach ($data as $key => $v) {
			$update[]="$key = '$v'";
		}
		$update=implode(',', $update);
		
		$sql = "UPDATE $this->table SET $update WHERE id=$id";
		$result=mysqli_query($link,$sql);
		Db::close($link);
	}

	/**
	 * Xóa 1 bản ghi trong CSDL theo ID
	 * @param  [int] $id [ID bản ghi cần xóa]
	 * @return [BOOLEAN]     [TRUE|FALSE]
	 */
	public function delete($id)
	{
		$link=Db::getInstance();
		$sql = "DELETE FROM $this->table WHERE id=$id";


		$result=mysqli_query($link,$sql);
		Db::close($link);
	}

}
?>