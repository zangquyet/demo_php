<?php 
class User extends Base_model
{
	/**
	 * Bảng trong CSDL
	 * @var string
	 */
	protected  $table='users';


	public function get_All()
	{
		$link=Db::getInstance();
		$sql="SELECT $this->table.*,province_name FROM $this->table LEFT JOIN provinces ON $this->table.province_id=provinces.id";
		$result=mysqli_query($link,$sql);
		Db::close($link);
		$list=array();
		while ($row=mysqli_fetch_assoc($result)) {
			$list[]=$row;
		}
		return $list;	
	}
}
?>